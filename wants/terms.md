# Требования (условия)

[[_TOC_]]

## Настройки

Надо иметь настройки вида JSON по тому, какие реквизиты каких объектов являются контролируемыми

Итоговый запрос (SQL) надо строить в динамике относительно этих реквизитов

[Пример настроечного JSON](./settings.json)

Все настройки ***обязательно*** под версионированием!

## Выгрузка

* Многопоточная
* Формирует файлы CSV пригодные для ***BULK INSERT*** в SQL. Структура CSV должна повторять [структуру итоговой таблицы](./data_structure.md)
* Формат имен файлов yyyyMMdd-yyyyMMdd-<Имя базы>-<Порядковый номер файла nnnnnnnn>.csv
* Надо иметь контроль за количеством *Объектов* в одном файле выгрузки

## Загрузка

Загрузку производим через BULK INSERT

* При загрузке надо контролировать, чтобы объекты не "приехали" из другого периода, например такой порядок загрузки
  * удаляем данные загружаемого периода
  * грузим все в другую таблицу
  * смотрим, есть ли в основной таблице объекты которые загрузили
  * по ним делаем пометку в логе загрузки, из какого периода они приехали, какой период соответственно попутно изменился
  * удаляем эти дубликаты загруженного в основной таблице
  * копируем загруженное в основную таблицу
  * чистим таблицу загрузки (копию основной по структуре)
* Ведем лог загрузки (CI?)

## Запрос на выборку

Строится в динамике по настройкам. Где строится и кто выводит результат неважно, итоговых расхождений не должно быть слишком много. В крайнем случае расхождения можно бить по вариантам и записывать отдельной выводимой таблицей.

[Пример запроса на 1С](./query_1c.md)

### Варианты ошибок

1. Документ НЕ Проведен + Документ Проведен - для документов
2. Разница дат документов - документы равны, периоды различны - для документов
3. Ссылки равны, реквизитный состав шапки различен
4. Ссылки равны, реквизитный состав таблиц различен
5. Сведение данных - данные объектов равны, при этом ссылки различны
6. Оставшиеся "лишние" объекты первой базы
7. Оставшиеся "лишние" объекты второй базы

### План запроса

Для выборки данных каждого следующего варианта ошибки используем только остаток данных после выборки предыдущего варианта.

* Сводим корректные данные - все реквизиты объектов по группировке ссылок равны. Остаток = то, что не сошлось (все ошибочные данные)
* Для остатка - выбираем с группировкой по Ссылка равно все кроме Проведен
* Для остатка - выбираем с группировкой по Ссылка различные шапки
* Для остатка - выбираем с группировкой по Ссылка различные таблицы
* Для остатка - данные объектов равны, различны ссылки
* Для остатка - все, что осталось для первой базы
* Для остатка - все, что осталось для второй базы
* Для остатка - проверка, что все верно. остаток должен быть пустым